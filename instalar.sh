#!/usr/bin/env sh

######################################################################
# @author      : Javier FFebrero (javiferfeb@live.com)
# @file        : instalar
# @created     : sábado ago 06, 2022 01:56:02 CEST
#
# @description : 
######################################################################

GREEN="\033[0;32m"
END="\033[0m"

echo "___________   ___.    "
echo "\_   _____/___\_ |_________   ___________  ____  "
echo "|    __)/ __ \| __ \_  __ \_/ __ \_  __ \/  _ \ "
echo "|     \\  ___/| \_\ \  | \/\  ___/|  | \(  <_> )"
echo "\___  / \___  >___  /__|    \___  >__|   \____/ "
echo "    \/      \/    \/            \/              "


PRG=$0
PARAM=$1
DIR="$( cd "$( dirname "$0" )" && pwd )"


## Variables


## Ayuda                                                                       #
Help()
{
   # Display Help
   echo "Agrega una descripción de las funciones del script aquí."
   echo
   echo "Sintaxis: scriptTemplate [-h|up|scripts|zsh|todo]"
   echo "Opciones:"
   echo "h        Imprime esta ayuda."
   echo "up       Crea la estructura de carpetas."
   echo "scripts  Carga los scripts."
   echo "zsh      Configura la terminal con zsh."
   echo "todo     Configura todo."
   echo
}


launch_up_carpetas()
{
  echo "${GREEN}Arrancando todo...${END}"
  echo "${GREEN}Creando carpeta personal $USER${END}"
  echo "${GREEN}Esto tomará un momento...${END}"
  
  cd $HOME
  mkdir -p kk
  mkdir -p .$USER/Proxectos
  mkdir -p .$USER/scripts
  mkdir -p .$USER/senhas
  mkdir -p .$USER/ficheiros
  
  echo "${GREEN}Estructura de carpetas creada para $USER${END}"
}

launch_up_scripts()
{
  cd $HOME/.$USER/scripts
  
  echo "${GREEN}Clonando los scripts...${END}"
  git clone https://gitlab.com/FFebrero/scripts.git
  
  echo "${GREEN}Scripts cargados.${END}"
  
  export PATH="$PATH:$HOME/.$USER/scripts/scripts"
  
  cp $HOME/.$USER/scripts/scripts/senha $HOME/.$USER/senhas
  
  echo "${GREEN}Archivo de contraseñas añadido.${END}"
  
  echo "Scripts y estructura cargados..."
}

launch_up_zsh()
{
  echo "${GREEN}Configurando Shell Zsh y temas...${END}"
  
  if [ $(uname) = $LINUX ]; then
    if [ -d ~/.zshrc ]; then
      sudo apt install git curl -y
      sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    else
      sudo apt update && sudo apt install zsh
      sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi
  fi
  
  if [ $(uname) = $DARWIN ]; then
    if [ -d ~/.zshrc ]; then
      sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    else
      brew install zsh
      sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    fi
  fi
  
  echo "${GREEN}Configurado el PATH y los Alias.${END}"
  
  cd $HOME
  
  if [ -f $HOME/.zshrc ]; then
    cd $HOME
    echo " " >> .zshrc
    echo "# Añadimos Scripts al Path" >> .zshrc
    echo 'export PATH="$PATH:$HOME/.$USER/scripts/scripts"' >> .zshrc
  elif [[ -f $HOME/.bashrc ]]; then
    cd $HOME
    echo " " >> .bashrc
    echo "# Añadimos Scripts al Path" >> .bashrc
    echo 'export PATH="$PATH:$HOME/.$USER/scripts/scripts"' >> .bashrc
  else
    echo "Ni bash ni zsh, no se puede añadir el Path de los scripts."
  fi
  
  echo "Copiamos archivos Zsh y añadimos repo en Proxectos$USER"
  cd $HOME/.$USER/Proxectos
  
  if git clone https://github.com/fdezfebrero/bash_zsh.git; then
    bash_zsh -g
  else
    echo "Error al cargar el archivo ZSH."
  fi
  
  echo "Configurada la terminal con Zsh."
}

check_installation()
{
  command -v $1 >/dev/null 2>&1
}

install_vim()
{
  echo "${GREEN}Instalando Vim...${END}"
  
  if check_installation vim; then
    echo "Vim ya está instalado."
  else
    if [ $(uname) = $LINUX ]; then
      sudo apt install vim -y
    elif [ $(uname) = $DARWIN ]; then
      brew install vim
    fi
    
    echo "${GREEN}Vim instalado.${END}"
  fi
}

install_neovim()
{
  echo "${GREEN}Instalando Neovim...${END}"
  
  if check_installation nvim; then
    echo "Neovim ya está instalado."
  else
    if [ $(uname) = $LINUX ]; then
      sudo apt install neovim -y
    elif [ $(uname) = $DARWIN ]; then
      brew install neovim
    fi
    
    echo "${GREEN}Neovim instalado.${END}"
  fi
}

install_nvchad()
{
  echo "${GREEN}Instalando NVChad...${END}"
  
  if [ -d ~/.config/nvim ]; then
    echo "NVChad ya está instalado."
  else
    echo "${GREEN}Clonando NVChad...${END}"
    git clone https://github.com/NvChad/NvChad ~/.config/nvim
    cd ~/.config/nvim || exit
    
    echo "${GREEN}Instalando plugins de NVChad...${END}"
    nvim --headless +PackerSync +qall
    
    echo "${GREEN}NVChad instalado.${END}"
  fi
}

## Comprobar e instalar Curl si es necesario
check_curl() {
  if ! command -v curl >/dev/null 2>&1; then
    echo "Curl no está instalado. Instalando Curl..."
    if [ "$(uname)" = "Linux" ]; then
      sudo apt update
      sudo apt install curl -y
    elif [ "$(uname)" = "Darwin" ]; then
      brew install curl
    fi
    echo "Curl instalado correctamente."
  else
    echo "Curl ya está instalado."
  fi
}

## Comprobar e instalar Git si es necesario
check_git() {
  if ! command -v git >/dev/null 2>&1; then
    echo "Git no está instalado. Instalando Git..."
    if [ "$(uname)" = "Linux" ]; then
      sudo apt update
      sudo apt install git -y
    elif [ "$(uname)" = "Darwin" ]; then
      xcode-select --install
    fi
    echo "Git instalado correctamente."
  else
    echo "Git ya está instalado."
  fi
}

## Comprobar e instalar dependencias iniciales
check_dependencies() {
  check_curl
  check_git
}


####
####
# TODO: Cargar configuración de Qtile, Vim, dunst...
###
###




## Script                                                              #
# Opciones
[[ $# -lt 1 ]] && Help && exit 1

option=$1

case $option in
      h)
         Help
         exit;;
      up)
        check_dependencies
        launch_up_carpetas
        exit 1;;
      scripts)
        check_dependencies
        launch_up_scripts
        exit;;
      zsh)
        check_dependencies
        launch_up_zsh
        exit 1;;
      todo) 
        check_dependencies
        launch_up_carpetas
        launch_up_scripts
        launch_up_zsh
        install_vim
        install_neovim
        install_nvchad
        exit 1;;
     \?)
         echo "Error"
         exit;;
esac

Help && exit 1
