Las opciones disponibles son:

    up: Crea la estructura de carpetas necesaria.
    scripts: Carga los scripts personalizados.
    zsh: Configura la terminal con Zsh y temas.
    todo: Realiza todas las tareas anteriores.

Requisitos

El script asume que tienes los siguientes requisitos en tu sistema:

    Git: para clonar repositorios.
    Curl: para descargar archivos desde Internet.
    Vim (opcional): si no está instalado, el script intentará instalarlo.
    Neovim (opcional): si no está instalado, el script intentará instalarlo.
    NVChad (opcional): si no está instalado, el script clonará el repositorio oficial y realizará la configuración inicial.

Personalización

Si deseas personalizar el script para adaptarlo a tus necesidades, puedes editar las funciones y agregar o eliminar tareas según sea necesario.

Recuerda que este script está destinado a un entorno específico y puede requerir ajustes adicionales según tu sistema y preferencias.
Contribución

Si deseas contribuir a este proyecto, siéntete libre de abrir un problema o enviar una solicitud de extracción. ¡Tus contribuciones son bienvenidas!
Autor

    Nombre: Javier FFebrero
    Correo electrónico: javiferfeb@live.com

Descripción detallada del script

Este script está escrito en shell y tiene como objetivo simplificar la instalación y configuración de varias herramientas y configuraciones en un sistema. A continuación se proporciona una descripción más detallada de las funciones y tareas realizadas por el script:
Estructura de carpetas

La función launch_up_carpetas crea una estructura de carpetas en el directorio del usuario. Las carpetas creadas incluyen "Proyectos", "scripts", "senhas" y "ficheiros".
Carga de scripts

La función launch_up_scripts clona un repositorio de scripts personalizados y los carga en la carpeta "scripts" creada anteriormente. También copia un archivo de contraseñas en la carpeta "senhas".
Configuración de Zsh

La función launch_up_zsh configura la terminal con Zsh y temas. Primero verifica si Zsh está instalado y, de ser necesario, lo instala. Luego, configura el archivo de inicio de Zsh (.zshrc o .bashrc) para agregar el directorio de scripts al PATH. A continuación, clona un repositorio con archivos de configuración adicionales para Zsh y realiza los ajustes necesarios.
Verificación e instalación de Vim y Neovim

Las funciones install_vim e install_neovim verifican si Vim y Neovim están instalados, respectivamente. Si no lo están, intentan instalarlos utilizando los paquetes disponibles en el sistema operativo.
Verificación e instalación de NVChad

La función install_nvchad verifica si NVChad está instalado. Si no lo está, clona el repositorio oficial de NVChad y realiza la configuración inicial de los complementos.

Este script es proporcionado "tal cual", sin garantías de ningún tipo. Úsalo bajo tu propio riesgo y asegúrate de revisar y comprender el código antes de ejecutarlo en tu sistema.

Si tienes alguna pregunta o encuentras algún problema, no dudes en contactar al autor utilizando los detalles de contacto proporcionados anteriormente.

¡Espero que este script te sea útil y simplifique tu proceso de instalación y configuración! Siéntete libre de personalizarlo y mejorarlo según tus necesidades.